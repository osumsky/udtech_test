package com.somsoft.test_udtech.retrofit2;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JSONPlaceHolderApi {

    // https://api.thecatapi.com/v1/breeds
    @GET("breeds")
    Call<List<Breed_Title>> getBreeds();

    // https://api.thecatapi.com/v1/images/search?breed_id=abys
    @GET("images/search")
    Call<List<Breed_Info>> getBreedById(@Query("breed_id") String breed_id);

}
