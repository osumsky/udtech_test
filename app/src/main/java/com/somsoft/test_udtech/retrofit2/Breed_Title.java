package com.somsoft.test_udtech.retrofit2;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Breed_Title implements Serializable {

    /*
            "id":"abys",
            "name":"Abyssinian",
            "origin": "Egypt",
            "description": "The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.",
            "wikipedia_url": "https://en.wikipedia.org/wiki/Abyssinian_(cat)",
     */

    @SerializedName("id")
    private String breedId;

    @SerializedName("name")
    private String breedName;

    @SerializedName("description")
    private String breedDescription;

    @SerializedName("origin")
    private String breedOrigin;

    @SerializedName("wikipedia_url")
    private String wiki_url;

    public String getBreedId() {
        return breedId;
    }

    public String getBreedName() {
        return breedName;
    }

    public String getBreedDescription() {
        return breedDescription;
    }

    public String getBreedOrigin() {
        return breedOrigin;
    }

    public String getWiki_url() {
        return wiki_url;
    }
}
