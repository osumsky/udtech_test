package com.somsoft.test_udtech;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.somsoft.test_udtech.retrofit2.Breed_Info;
import com.somsoft.test_udtech.retrofit2.Breed_Title;
import com.somsoft.test_udtech.retrofit2.NetworkService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.somsoft.test_udtech.MainActivity.BREED_KEY;

public class BreedActivity extends AppCompatActivity {

    LinearLayout mainLayout;
    TextView tvName;
    TextView tvDesc;
    TextView tvOrigin;
    ImageView catImage;
    Breed_Title breed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breed);
        mainLayout = findViewById(R.id.breed_layout);

        Intent i = getIntent();
        breed = (Breed_Title) i.getSerializableExtra(BREED_KEY);

        catImage = findViewById(R.id.catImage);
        loadImage(getApplicationContext(), breed.getBreedId());
        tvName = findViewById(R.id.tvName);
        tvName.setText(breed.getBreedName());
        tvOrigin = findViewById(R.id.tvOrigin);
        tvOrigin.setText(breed.getBreedOrigin());
        tvDesc = findViewById(R.id.tvDescription);
        tvDesc.setText(breed.getBreedDescription());
    }

    // Наполняем список данными с сервера
    private void loadImage(Context context, String id) {
        NetworkService.getInstance()
                .getJSONApi()
                .getBreedById(id)
                .enqueue(new Callback<List<Breed_Info>>() {
                    @Override
                    public void onResponse(Call<List<Breed_Info>> call, Response<List<Breed_Info>> response) {
                        List<Breed_Info> data = response.body();
                        if (data != null) {
                            Glide.with(context)
                                    .load(data.get(0).getUrl())
                                    .override(200, 200)
                                    .placeholder(R.drawable.no_image)
                                    .into(catImage);
                        } else {
                            Snackbar.make(mainLayout, getString(R.string.noData), Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Breed_Info>> call, Throwable t) {
                        Snackbar.make(mainLayout, getString(R.string.error), Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    public void onClick(View v) {
        Uri webpage = Uri.parse(breed.getWiki_url());
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Snackbar.make(mainLayout, "Не найдено приложение для открытия ссылки", Snackbar.LENGTH_LONG).show();
        }
    }


}
