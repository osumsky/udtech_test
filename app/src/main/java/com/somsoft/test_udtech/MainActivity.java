package com.somsoft.test_udtech;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import com.somsoft.test_udtech.retrofit2.Breed_Title;
import com.somsoft.test_udtech.retrofit2.NetworkService;

import java.io.Serializable;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RVAdapter adapter;
    List<Breed_Title> breedTitles;
    LinearLayout main_layout;
    ProgressDialog progressDialog;
    static final String BREED_KEY = "BREED";
    static final String LIST_KEY = "LIST";

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        main_layout = findViewById(R.id.main_layout);

        if (savedInstanceState != null) {
            breedTitles = (List<Breed_Title>) savedInstanceState.getSerializable(LIST_KEY);
            initRV();
        } else {
            // Progress Dialog
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
            // Проверяем наличие интернет соедениения
            ReactiveNetwork
                    .observeNetworkConnectivity(getApplicationContext())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(connectivity -> {
                                // Если есть: заполняем список данными с сервера, иначе - выдаем ошибку
                                if (connectivity.state() == NetworkInfo.State.CONNECTED) {
                                    populateList();
                                } else {
                                    progressDialog.dismiss();
                                    Snackbar.make(main_layout, getString(R.string.noConnection), Snackbar.LENGTH_SHORT).show();
                                }
                            }
                    );
        }

    }

    // Создаем список RecyclerView
    private void initRV() {
        recyclerView = findViewById(R.id.recycleView);
        LinearLayoutManager recyclerViewLayout = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewLayout);
        adapter = new RVAdapter(breedTitles);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
    }

    // Наполняем список данными с сервера
    private void populateList() {
        NetworkService.getInstance()
                .getJSONApi()
                .getBreeds()
                .enqueue(new Callback<List<Breed_Title>>() {
                    @Override
                    public void onResponse(Call<List<Breed_Title>> call, Response<List<Breed_Title>> response) {
                        breedTitles = response.body();
                        if (breedTitles != null) {
                            setTitle(getString(R.string.app_name) + " (" + breedTitles.size() + ")");
                            initRV();
                        } else {
                            Snackbar.make(main_layout, getString(R.string.noData), Snackbar.LENGTH_SHORT).show();
                        }
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<List<Breed_Title>> call, Throwable t) {
                        progressDialog.dismiss();
                        Snackbar.make(main_layout, getString(R.string.error), Snackbar.LENGTH_LONG).show();
                    }
                });
    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(LIST_KEY, (Serializable) breedTitles);
    }

}